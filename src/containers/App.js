import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as TodoActions from '../actions'
import Intro from '../components/intro'

const App = (todos, actions) => <Intro todos={todos} actions={actions} />

const mapStatetoProps = state => ({
  todos: state.todos,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TodoActions, dispatch),
})

export default connect(mapStatetoProps, mapDispatchToProps)(App)
