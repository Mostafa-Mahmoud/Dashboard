import {ADD_TODO} from "../constants/ActionTypes"

const initialState = [
  {
    id: 0,
    completed: false,
  },
]
const todos = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {}
    default:
      return state
  }
}

export default todos
