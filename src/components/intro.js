import React, {Component} from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

// import Chart from './chart'
import Start from './start'

export default class Intro extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Start />
      </MuiThemeProvider>
    )
  }
}
