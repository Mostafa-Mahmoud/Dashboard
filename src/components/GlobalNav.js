import React, {PureComponent} from 'react'
import AtlassianIcon from '@atlaskit/icon/glyph/atlassian'
import SearchIcon from '@atlaskit/icon/glyph/search'
import QuestionCircleIcon from '@atlaskit/icon/glyph/question-circle'
import AddIcon from '@atlaskit/icon/glyph/add'
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left'
import Avatar from '@atlaskit/avatar'
import InlineDialog from '@atlaskit/inline-dialog'
import Tooltip from '@atlaskit/tooltip'
import {colors} from '@atlaskit/theme'
import Navigation, {
  createGlobalTheme,
  presetThemes,
  AkContainerTitle,
  AkNavigationItemGroup,
  AkNavigationItem,
  AkSearchDrawer,
  AkCreateDrawer,
  AkGlobalItem,
  AkContainerNavigationNested,
} from '@atlaskit/navigation'

const containerTheme = createGlobalTheme(colors.N20, colors.P400)
const globalTheme = createGlobalTheme(colors.P400, colors.N20)

class Start extends React.PureComponent {
  state = {
    isOpen: true,
  }

  onResize = ({isOpen}) => {
    this.setState({isOpen})
  }

  render() {
    return (
      <Navigation
        containerTheme={containerTheme}
        globalTheme={globalTheme}
        onResize={this.onResize}
        isOpen={this.state.isOpen}
        globalPrimaryIcon={<AtlassianIcon size="xlarge" label="Atlassian" />}
        globalPrimaryItemHref="/components/navigation"
        containerHeaderComponent={() => (
          <AkContainerTitle
            icon={<AtlassianIcon label="Atlassian" />}
            text="Example Navbar"
          />
        )}
      >
        <AkNavigationItemGroup>
          <AkNavigationItem text="Nav Item" href="/components/navigation" />
          <AkNavigationItem
            text="Selected Item"
            isSelected
            href="/components/navigation"
          />
        </AkNavigationItemGroup>
      </Navigation>
    )
  }
}

export default Start
